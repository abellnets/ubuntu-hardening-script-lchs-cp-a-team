#!/bin/bash
bash ./binbert_script/binbert_script_chkconfigs.sh
bash ./binbert_script/binbert_script_banner.sh
bash ./binbert_script/binbert_script_inittab.sh
bash ./binbert_script/binbert_script_ssh_config.sh
bash ./binbert_script/binbert_script_sysctl.sh

# Ignoring the next part... already done with main script!
#if [ $(id -u) -eq 0 ]; then
#read -p "Enter username : " username
#read -s -p "Enter password : " password
#egrep "^$username" /etc/passwd >/dev/null
#if [ $? -eq 0 ]; then
#echo "$username exists!"
#exit 1
#else
#pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
#useradd -m -p $pass $username
#[ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
#fi
#else
#echo "Only root may add a user to the system"
#exit 2
#fi
