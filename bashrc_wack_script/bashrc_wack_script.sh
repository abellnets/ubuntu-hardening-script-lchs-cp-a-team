#!/bin/bash
#echo "This script can be used for RHEL hardening"
#echo " "
#sleep 2

# IGNORING PART 1: Apt-get isn't RPM nor YUM... I'll try to convert this later.
#echo "1. checking some basic packages which should not be installed:"
#for package in inetd xinetd ypserv tftp-server telnet-server rsh-serve
#do
#      if ! rpm -qa | grep $package >& /etc/null;
#      then
#      echo "package $package is not installed"
#      else
#      echo "The $package is installed. Erasing it now."
#      yum erase $package
#      fi
#done
#sleep 2
#echo " "

# See BinBert script
#echo "2. Checking SElinux settings:"
#x=`cat /etc/sysconfig/selinux | grep ^SELINUX | head -n 1 | awk -F= '{print $2}'`
#if [ $x == disabled ]
#then
#    echo "SElinux is disabled"
#    echo "Changing it to enforcing"
#    sed -i 's/^SELINUX=disabled/SELINUX=enforcing/' /etc/sysconfig/selinux
#else
#    echo "SElinux is already in enforcing mode"
#fi
#sleep 2
#echo " "

bash ./bashrc_wack_script/bashrc_wack_script_password_policies.sh

#echo "Restricting use of previous passwords:"
#echo "None applied"
#sleep 2

bash ./bashrc_wack_script/bashrc_wack_script_passwordless.sh

bash ./bashrc_wack_script/bashrc_wack_script_uid0.sh

#See BinBert script
#echo "6. Disabling root login in system"
#sed -i '/^#PermitRootLogin/a PermitRootLogin no' /etc/ssh/sshd_config
#sed -i 's/^#Port 22/Port 2222/' /etc/ssh/sshd_config
#sleep 2

bash ./bashrc_wack_script/bashrc_wack_script_sysctl.sh

bash ./bashrc_wack_script/bashrc_wack_script_chmod.sh

#echo "Disk partitions:"
#echo "***None applied yet***"
#sleep 2

#echo "disabling IPv6:"
#echo "None applied"
#sleep 2

#echo "creating GRUB password:"

#echo "use of gconftool"

#echo "write verify script"

bash ./bashrc_wack_script/bashrc_wack_script_passwd.sh

bash ./bashrc_wack_script/bashrc_wack_script_banner.sh
