#!/bin/bash
#echo "7. Linux kernel hardening:"
cp /etc/sysctl.conf /etc/sysctl.conf.backup
echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.forwarding = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.mc_forwarding = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.default.log_martians = 1" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_syn_backlog = 4096" >> /etc/sysctl.conf
echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
#sleep 2
#echo "Changes in /etc/sysctl.conf file are done."
#sleep 1
#echo " "
