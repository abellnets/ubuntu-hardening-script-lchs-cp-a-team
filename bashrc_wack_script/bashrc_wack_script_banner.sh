#!/bin/bash
#echo "10. Setting 'Banner' and 'Motd'"
echo "*****************************************************************************" > /etc/motd
echo -e "!!!WARNING!!!\n" >> /etc/motd
echo " This System is for the use of authorized company personnel only and by accessing this system  you here by consent to the system being monitored by the company. Any unauthorized use will be considered a breach of company's Information Security Policies and may be unlawful under law. " >> /etc/motd
echo "*****************************************************************************" >> /etc/motd
cp /etc/issue /etc/issue.net
#cp /etc/issue /etc/motd
#echo "Banner is set."

