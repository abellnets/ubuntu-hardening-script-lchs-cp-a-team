#!/bin/bash
#echo "8. Setting permissions to restrictive for commonly used commands"
chmod 100 /bin/rpm
chmod 100 /bin/tar
chmod 100 /bin/gzip
chmod 100 /bin/ping
chmod 100 /bin/gunzip
chmod 100 /bin/mount
chmod 100 /bin/umount
chmod 100 /usr/bin/gzip
chmod 100 /usr/bin/gunzip
chmod 100 /usr/bin/who
chmod 100 /usr/bin/lastb
chmod 100 /usr/bin/last
chmod 100 /usr/bin/lastlog
chmod 100 /sbin/arping
#chmod 100 /usr/sbin/traceroute

#chmod 400 /etc/syslog-ng/syslog-ng.conf
chmod 400 /etc/hosts.allow
chmod 400 /etc/hosts.deny
#chmod 400 /etc/sysconfig/syslog
chmod 644 /var/log/wtmp
#echo "commands permissions changed"
#sleep 1
#echo " "
