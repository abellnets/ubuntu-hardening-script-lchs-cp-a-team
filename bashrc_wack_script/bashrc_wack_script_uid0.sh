#!/bin/bash
#echo "5. Checking if No Non-Root Accounts Have UID Set To 0:"
x=`awk -F: '($3 == "0") {print}' /etc/passwd | awk -F: '{print $1}'`
if [ $x == root ]
then 
#echo "No account other than ROOT has UID 0"
	echo ""
else 
echo "***** Check the file. More than one accounts have UID 0"
fi
#sleep 2
#echo " "
