#!/bin/bash
#echo "3. Changing different parameters of password aging"
sed -i '/^PASS_MAX_DAYS/c\PASS_MAX_DAYS  60' /etc/login.defs
sed -i '/^PASS_MIN_DAYS/c\PASS_MIN_DAYS  7' /etc/login.defs # Modified from "1" to correspond with CP recommendations.
sed -i '/^PASS_MIN_LEN/c\PASS_MIN_LEN   8' /etc/login.defs
sed -i '/^PASS_WARN_AGE/c\PASS_WARN_AGE   14' /etc/login.defs # Modified from "15"... who uses non-round numbers???
#echo "Changes in /etc/login.defs file are done"
#sleep 2
#echo " "
