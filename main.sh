#!/bin/bash

# TODO: compare_users() not set up to read groups.... almost finished. Fix menu (if still needed), etc.

script_version=".998013"
# Script made by Hross for Liberty Common High School CyberPatriot Alpha team, 2014-2015
# I'll try to document the code the best I can -- I'm quite rusty with scripting and programming in general... It's been years, give me a break!
# --------------------------------------------------------------------------------
# First, I'm going to make sure this is running as root (either by root console or by sudo) 
if [ ! $(whoami) = "root" ] ; then
	echo "Get out and RUN THIS WITH ROOT PERMISSIONS! NOW!"
	exit 1
fi
# Second, I'm going to output the greeting and disclaimer... bad programming practice, better psychotherapy practice ;)
clear
echo "Welcome to the Liberty Common High School CyberPatriot Alpha team Linux script, version $script_version!"
echo "This is written in bash scripting, designed for Ubuntu 12.04."
echo "It may not work any other flavour of Linux or version of Ubuntu."
echo "Note: This script has module-based parts. Please modify the corresponding files."
echo "Return 1 to start script, anything else will close it."
read case_enter;
case $case_enter in
	1) echo "" &> /dev/null;; # This is technically bad practice, but for bash scripting, it may be for the best...
	*) exit 0;; # Exits, no problem. Not everybody likes my code...
esac

# For those following the flow of this program, go down to the end-ish of the file, minus a couple of lines.
# It will call the following function (user_preli), return, prompt to ensure /etc/hosts is correct, return, and then call the main menu.

function contains_element_funct { # Taken from patrik's answer on http://stackoverflow.com/questions/3685970/check-if-an-array-contains-a-value
	local e
	for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
	return 1
}

function user_preli {
	# Code taken from http://www.cyberciti.biz/faq/linux-list-users-command/
	_l="/etc/login.defs"
	_p="/etc/passwd"
	## get mini UID limit ##
	l=$(grep "^UID_MIN" $_l) 
	## get max UID limit ##
	l1=$(grep "^UID_MAX" $_l)
	
	# Clean the strings to be a clean number.
	l_clean=$(tr -dc '0-9' <<< $l)
	l1_clean=$(tr -dc '0-9' <<< $l1)
	
	# Make an array of users with UID 0 and between MIN and MAX
	# This will be reset once the users are compared
	# echo `awk -F: '!/nobody/ { if ($3 >=1000) { print $1 }}' /etc/passwd`
	# Above command from bondhi zazen's comment on http://studyhat.blogspot.com/2010/10/find-out-non-system-users.html
	IFS=$'\n' read -d '' -r -a users_system_long < ./users_settings/users.system 
	#These users will be ignored from the list... 
	#I don't even check to see if they're real here, this is an archetypal list for base Ubuntu 12.04. 
	#UPDATE: next lines reading from field 1 only, 2 and 3 will be home and shell, respectively.
	users_system=($(echo "$(echo ${users_system_long[@]} | tr " " "\n" | awk -F: '{ print $1 }')" | tr " " "\n"))
	users_system_home=($(echo "$(echo ${users_system_long[@]} | tr " " "\n" | awk -F: '{ print $2 }')" | tr " " "\n"))
	users_system_shell=($(echo "$(echo ${users_system_long[@]} | tr " " "\n" | awk -F: '{ print $3 }')" | tr " " "\n"))
	unset users_system_long
	# Onto the current non-system users on the machine.
	users=($(echo "$(awk -v lowest_val=$l_clean -F: '!/nobody/ { if ($3 >=lowest_val) { print $1 }}' /etc/passwd)" | tr " " "\n"))
	users_home=($(echo "$(awk -v lowest_val=$l_clean -F: '!/nobody/ { if ($3 >=lowest_val) { print $6 }}' /etc/passwd)" | tr " " "\n"))
	users[${#users[@]}]='root'
	# Now taking the REAL system users except the REAL users
	# Doing this to ensure no users slipped through "at all" (instead of inverting the above command)
	real_users_system=()
	real_users_system_home=()
	real_users_system_shell=()
	real_users_system_name=()
	IFS=$'\n' read -d '' -r -a tmp_array < /etc/passwd
	for user_preli_ind in "${tmp_array[@]}" ; do
		contains_element_funct "$(echo $user_preli_ind | (awk -F: '{ print $1 }' | tr -d '#'))" "${users[@]}"
		if [ $? -eq 1 ] ; then # It isn't a "regular-ish user"
			real_users_system[${#real_users_system[@]}]="$(echo $user_preli_ind | (awk -F: '{ print $1 }' | tr -d '#'))"
			real_users_system_name[${#real_users_system_name[@]}]="$(echo $user_preli_ind | awk -F: '{ print $5 }')"
			real_users_system_home[${#real_users_system_home[@]}]="$(echo $user_preli_ind | awk -F: '{ print $6 }')"
			real_users_system_shell[${#real_users_system_shell[@]}]="$(echo $user_preli_ind | awk -F: '{ print $7 }')"
		fi
	done
	unset user_preli_ind
	for user_preli_ind in "${tmp_array[@]}" ; do
		if [ "$(echo $user_preli_ind | awk -F: '{ print $1 }')" == "root" ] ; then
			if [ "$(echo $user_preli_ind | awk -F: '{ print $3 }')" == "0" ] ; then
				stuff_about_root=($(echo $user_preli_ind | tr ':' ' ')) # Order of passwd file: user - pass_enc (not used, but there) - uid - dgid - name - home - shell
			fi
		fi
	done
	unset tmp_array
	unset user_preli_ind
}

function check_hosts_file {
	echo "Printing /etc/hosts file. Please make sure it is correct."
	echo " "
	cat /etc/hosts
	echo " "
	echo "Is this OK? [1/0]"
	read hosts_case;
	case $hosts_case in
		1) echo " " &> /dev/null;; # Euh... this again.
		y) echo " " &> /dev/null;;
		Y) echo " " &> /dev/null;;
		*) echo "Go and fix that before continuing with this script"
			exit 1;;
	esac
	unset hosts_case
	echo "Printing /etc/resolv.conf file. Please make sure it is correct."
	echo " "
	cat /etc/resolv.conf
	echo " "
	echo "Is this OK? [1/0]"
	read resolv_case;
	case $resolv_case in
		1) echo " " &> /dev/null;;
		y) echo " " &> /dev/null;;
		Y) echo " " &> /dev/null;;
		*) echo "Go and fix that before continuing with this script"
			exit 1;;
	esac
	unset resolv_case
}

function pass_pol_set {
	# Will not touch /etc/login.defs, etc. Will only modify (after what cracklib does and backing up) the /etc/pam.d/* files.
	apt-get install libpam-cracklib --yes
	cp -r /etc/pam.d/ /etc/BAK.pam.d.BAK/
	# Both common-auth and common-password are root:root 644
	rm /etc/pam.d/common-auth
	rm /etc/pam.d/common-password
	cp ./perfect_files/pam.d/common-auth /etc/pam.d/
	cp ./perfect_files/pam.d/common-password /etc/pam.d/
	chown root:root /etc/pam.d/common-auth
	chown root:root /etc/pam.d/common-password
	chmod 644 /etc/pam.d/common-auth
	chmod 644 /etc/pam.d/common-password
	
	if [ $menu_case -eq 1022 ] ; then
		change_passwords
	fi
}

function change_passwords {
	# This now also contains "chage" policies to be set.
	P455w0RD=$(cat "./users_settings/pass.change") &> /dev/null
	if [ ! -z $1 ] ; then
		echo -e $P455w0RD"\n"$P455w0RD | passwd $1
		chage -m 7 -M 90 -I 10 -W 14 $1
		return # Adding this to avoid the following loop (which could break something...)
	fi
	for pass_ind in "${users[@]}"; do
		echo -e $P455w0RD"\n"$P455w0RD | passwd $pass_ind
		chage -m 7 -M 90 -I 10 -W 14 $pass_ind
	done
	# Take UID 0 and between MIN and up (no longer MAX) UID users, change all of the passwords to $P455w0RD.
	# Don't hesitate and don't ask questions: This is MY machine!
	unset P455w0RD
	if [ $menu_case -eq 1022 ] ; then
		compare_users
	fi
}

function disable_user {
	# Used to disable a user as outlined in bashrc's wack script and, IFF in /home/, moves the user's home directory to /BAK.home.BAK/
	# IFF ! in /home/, outputs a notice.
	echo -e "Disabling the user \"$1\""
	passwd -ql $1
	if [ -z $2 ] ; then
		echo -e "\"$1\" disabled and home directory was untouched (see previous output)"
		return
	fi
	tempness=($(echo $2 | tr '/' ' ')) # hopefully the directories don't have a space...
	if [ ! "${tempness[0]}" == "home" ] ; then
		echo -e "Cannot move home directory ; not in /home! \nDo it manually..." 
		unset tempness
		return
	fi
	if [ ! ${#tempness[@]} -eq 2 ] ; then
		echo -e "Cannot move home directory ; deeper than /home/x! \nPerhaps there was a space in the directory?"
		unset tempness
		return
	fi
	unset tempness
	if [ ! -d /BAK.home.BAK/ ] ; then
		mkdir /BAK.home.BAK
	fi
	mv $2 /BAK.home.BAK
	echo -e "\"$1\" disabled and home directory $2 was moved to /BAK.home.BAK$2."
}

function disable_guest {
	echo "Disabling the GNOME-DM/LightDM Guest account..."
	dpkg -s gdm-guest-session &> /dev/null
	if [ $? -eq 0 ] ; then
		apt-get uninstall gdm-guest-session --yes
	fi
	if [ ! -d /usr/share/lightdm/lightdm.conf.d/ ] ; then # This system is the default 12.04 LTS layout
		echo -e "Moving entire /etc/lightdm/ to /etc/BAK.lightdm.BAK/ and copying in \"perfect\" files."
		mv /etc/lightdm/ /etc/BAK.lightdm.BAK/
		cp -r ./perfect_files/lightdm/ /etc/
		chmod 755 /etc/lightdm/
		chown root:root /etc/lightdm/
		chmod 644 /etc/lightdm/*
		chown root:root /etc/lightdm/*
	fi
	if [ -d /usr/share/lightdm/lightdm.conf.d/ ] ; then # This system has been updated past the defaults on 12.04 LTS
		echo "Backing up /etc/lightdm/ and /usr/share/lightdm/"
		mv /etc/lightdm/ /etc/BAK.lightdm.BAK/
		cp -r ./perfect_files/lightdm/ /etc/
		chmod 755 /etc/lightdm/
		chown root:root /etc/lightdm/
		chmod 644 /etc/lightdm/*
		chown root:root /etc/lightdm/*
		rm /etc/lightdm/lightdm.conf # 14.0x doesn't have this file... The following is used instead.
		cp -r /usr/share/lightdm /usr/share/BAK.lightdm.BAK
		sh -c 'printf "[SeatDefaults]\nallow-guest=false\n" >/usr/share/lightdm/lightdm.conf.d/50-no-guest.conf' # Taken from david6's answer on http://askubuntu.com/questions/62564/how-do-i-disable-the-guest-session
	fi
}

function set_users {
	# This function was branched off of the original compare_users()... this will set users array and disable unauthorized users and write over users.allowed with new users array. compare_users() will ask about the groups that each user is part of.
	echo "Backing up /etc/passwd, /etc/shadow, and /etc/group files to /etc/BAK.x.BAK"
	cp /etc/passwd /etc/BAK.passwd.BAK
	cp /etc/shadow /etc/BAK.shadow.BAK
	cp /etc/group /etc/BAK.group.BAK
	# If user was not in original user array, change its password with change_passwords $username...
	echo "NOTICE: Will prompt about groups after setting users."
	echo "Assuming \"root\" (UID 0) is part of this list. Added."
	new_users=()
	new_users[0]="root"
	for(( this_ind = 0; this_ind < $(expr ${#users[@]} - 1); this_ind++)) ; do # "root" was added manually, but "root" is now disabled
		echo "Is ${users[$this_ind]} an acceptable user (regardless of permissions)?"
		while true ; do
			read user_case;
        		case $user_case in
        	        	1) new_users[${#new_users[@]}]=${users[$this_ind]}
					break;;
        	        	y) new_users[${#new_users[@]}]=${users[$this_ind]}
					break;;
        	        	Y) new_users[${#new_users[@]}]=${users[$this_ind]}
					break;;
				0) disable_user ${users[$this_ind]} ${users_home[$this_ind]}
					break;;
				n) disable_user ${users[$this_ind]} ${users_home[$this_ind]}
					break;;
				N) disable_user ${users[$this_ind]} ${users_home[$this_ind]}
					break;;
        	        	*) echo "What?";;
        		esac
        		unset user_case
		done
	done
	for dem_sys_usaz in "${real_users_system[@]}" ; do
                contains_element_funct "$dem_sys_usaz" "${users_system[@]}"
                if [ $? -eq 0 ] ; then
			echo "NOTICE: $dem_sys_usaz is a system-like user that is not part of the ./users_settings/users.system file! You may want to check that out..."
                fi
        done
	unset dem_sys_usaz
	# Dumps $new_users into users.allowed file.
	IFS=$'\n'; echo "${new_users[*]}" > ./users_settings/users.allowed
	unset new_users
}

function compare_users {
	# It works... not like how I wanted it to, but it works for now.
	echo "Have you updated the ./users_settings/users.allowed file (correctly)? (answering without a 1/y/Y will start the prompted users/groups script and later update that file automatically)"
	read user1_case;
	case $user1_case in
		1) echo " " &> /dev/null;;
		y) echo " " &> /dev/null;;
		Y) echo " " &> /dev/null;;
		*) set_users;;
	esac
	#If you're here, users array has been properly set. We will now read users.allowed into the new users array.
	echo "Should I disable the GNOME-based Guest account (in LightDM/GDM)?"
	read guest_case;
	case $guest_case in
		1) disable_guest;;
		y) disable_guest;;
		Y) disable_guest;;
		*) echo " " &> /dev/null;;
	esac
	unset users
	#IFS=$'\n' read -d '' -r -a users < ./users_settings/users.allowed # I've commented this because it's a pointless "waste" (100 B) of memory.
#	echo "At this rate... go and check/edit /etc/group file by yourself. This script can't find a way to accurately do it per user yet!"
	# My head hurts, so I'm just going to make this work...
	echo "So... the script isn't able to parse correctly yet (in an attempt to stay pure to Bash and not involve other languages like Python)."
	echo "It is because of that that you need to do the following:"
	echo "The group name is the first value per line and the user(s) (if any) are the last, seperated by commas. Hit return to modify the groups."
	read my_groans;
        case $my_groans in
		*) nano /etc/group;;
        esac
	unset my_groans
	echo "Good. Now you'll be presented to double-check the /etc/passwd and /etc/shadow files (in that order). Ensure that the UIDs, primary GIDs, usernames, shells, and home directories are correct."
	echo "Also make sure that only 'root' has UID/GID of 0 and NO ONE ELSE."
	echo "For a rough outline of who a systemuser is (don't worry about them unless it shares information with 'root' at this rate...), look at ./users_settings/users.system"
	echo "Google is your friend."
	echo "Press return to start editing."
	read my_groans;
        case $my_groans in
                *) nano /etc/passwd
			nano /etc/shadow;;
        esac
        unset my_groans
	#echo ${#users[@]}

	# The following is how users array was built in user_preli():
	# users=($(echo "$(awk -v lowest_val=$l_clean -F: '!/nobody/ { if ($3 >=lowest_val) { print $1 }}' /etc/passwd)" | tr " " "\n"))
        # users[${#users[@]}]='root'
	#
	# It may help in the algorithms here

	# NOTES: The best thing to do here may be to make a temporary file with users in users array, use Python to parse the /etc/group file, list in temp file which groups the user is part of, open file with nano to allow the competitor to modify it, parse it again (Python again?), and execute the correct code following that.

	# Print groups each user is in.
	if [ $menu_case -eq 1022 ] ; then
                cron_jobs_funct
        fi
}

function cron_jobs_funct {
	echo -e "Backing up /etc/crontab and /etc/anacrontab ; replacing with \"perfect\" files."
	mv /etc/anacrontab /etc/BAK.anacrontab.BAK
	mv /etc/crontab /etc/BAK.crontab.BAK
	cp ./perfect_files/anacrontab /etc/
	cp ./perfect_files/crontab /etc/
	chmod 644 /etc/anacrontab
	chmod 644 /etc/crontab
	echo "Printing Cron Jobs for each user... Will prompt to delete."
	for cron_ind in "${users[@]}"; do
		# Remember, remember the Fifth of November? That was the day I fixed this code to the following:
		crontab -u $cron_ind -l &> /dev/null
		if [ $? -eq 0 ] ; then # OMGosh it found something, you guys!
			# grep -v '^#' crontab -u $cron_ind -l ## Not working?
			echo "$cron_ind has this:   $(crontab -u $cron_ind -l | grep -v '^#')"
			echo " "
			echo "Can I get rid of this for you? [1 or y / *!{1|y|Y} or anthing not.... yes stuff]"
			echo "If you say no but you do want it gone / edited, you can run one of the following commands verbatim:"
			echo "REMOVAL: # crontab -u $cron_ind -r"
			echo "EDITING: # crontab -u $cron_ind -e"
			echo "So... can I get rid of this entire crontab from $cron_ind?"
			read cron_case;
                	case $cron_case in
				1) crontab -u $cron_ind -r ;;
				y) crontab -u $cron_ind -r ;;
				Y) crontab -u $cron_ind -r ;;
				*) echo "\"Prepare for unforeseen consequences!\" -Half Life 2: Episode Two" ;;
			esac
		else
			echo "$cron_ind's crontab looks OK!"
		fi
	done
	# Print every cronjob.
	# Offer to clean any cronjobs that exist.
	
	if [ $menu_case -eq 1022 ] ; then
                secure_firewall
        fi
}

function secure_firewall {
	echo "Copying \"perfect\" hosts.{allow&deny} files into /etc, making BAK.*.BAK of both existing"
	mv /etc/hosts.allow /etc/BAK.hosts.allow.BAK
	mv /etc/hosts.deny /etc/BAK.hosts.deny.BAK
	cp ./perfect_files/hosts/hosts.deny /etc/
	cp ./perfect_files/hosts/hosts.allow /etc
	chmod 644 /etc/hosts.allow
	chmod 644 /etc/hosts.deny
	chown root:root /etc/hosts.deny
	chown root:root /etc/hosts.allow
	# Secures the firewall using ufw... a front-end of iptables
	# More on iptables: http://www.cyberciti.biz/tips/linux-iptables-examples.html
	# Resets ufw to default, enables ufw, and sets the logging level to high
	echo "Resetting and enabling firewall (ufw)"
	echo -e "y\n" | ufw reset
	ufw enable
	ufw logging high
	# print status numbered
	while true ; do
		ufw status numbered
		echo -e "\n This is what the firewall looks like now. Add a new rule? [1/0]"
		read add_rule_case;
		case $add_rule_case in
			1) add_fw_rule ;;
			y) add_fw_rule ;;
			Y) add_fw_rule ;;
			*) break ;;
		esac
	done

	if [ $menu_case -eq 1022 ] ; then
                update_system
        fi
}

function add_fw_rule {
	while true ; do
		echo "Type \"allow\" or \"deny\" with port or protocol name followed by / and TCP or UDP to add rule."
		echo "EX: \"deny 80/tcp\" without quotes."
		echo "Type exit to go back to script."
		echo " "
		read fw_rule;
		if [ $fw_rule -eq "exit" ] ; then
			break
		fi
		ufw $fw_rule
	done
}

function update_system {
	echo "Moving /etc/apt/ to /etc/BAK.apt.BAK/ and adding \"perfect\" apt dir."
	mv /etc/apt /etc/BAK.apt.BAK
	cp -r ./perfect_files/apt /etc/apt
	chmod 755 /etc/apt/
	chmod 755 /etc/apt/*
	chmod 644 /etc/apt/sources.list
	chmod 644 /etc/apt/sources.list.save
	chmod 600 /etc/apt/trustdb.gpg
	chmod 644 /etc/apt/trusted.gpg
	chown -R root:root /etc/apt/ # OMG, I'm assuming root:root is a good guy... shouldn't be a problem in Round 2&3, at least. 
	
	echo "Updating database -> updating all software. Will not dist-upgrade for CP wishing to keep LTS."
	echo "Assuming yes to every check."
	apt-get update
	apt-get upgrade --yes
	apt-get autoremove --yes
	
	if [ $menu_case -eq 1022 ] ; then
                binbert_script
        fi
	if [ $menu_case -eq 137 ] ; then
		system_upgrade
	fi
}

function system_upgrade {
	echo "This function was selected manually. It will upgrade system."
	apt-get dist-upgrade --yes
#	do-release-upgrade #Almost resets half the system... avoiding.
}

function binbert_script {
	# Take ideas from http://www.binbert.com/blog/2011/01/redhat-linux-hardening/
	echo BinBert Script
	apt-get install sysv-rc-conf
	bash ./binbert_script/binbert_script.sh

	if [ $menu_case -eq 1022 ] ; then
                bashrc_wack_script
	fi
}

function bashrc_wack_script {
	#Taken AND MODIFIED from http://www.bashrc.in/2013/05/rhel-6-hardening-shell-script.html
	echo "Running Bashrc.in's wack script"

	bash ./bashrc_wack_script/bashrc_wack_script.sh

	if [ $menu_case -eq 1022 ] ; then
                lock_and_screensaver
	fi
}

function lock_and_screensaver {
	echo "Setting lock and auto logout."
	gsettings set org.gnome.desktop.screensaver logout-enabled true
	gsettings set org.gnome.desktop.screensaver logout-delay 300
	gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
	gsettings set org.gnome.desktop.screensaver lock-enabled true
	gsettings set org.gnome.desktop.screensaver lock-delay 60
	
	if [ $menu_case -eq 1022 ] ; then
		exp_and_inetd
	fi
}

function exp_and_inetd {
	if [ -d /etc/exports/ ] ; then
		mv /etc/exports/ /etc/BAK.exports.BAK/
		echo "Moved /etc/exports/ to BAK set."
	fi
	if [ -d /etc/inetd.d/ ] ; then
		mv /etc/inetd.d/ /etc/BAK.inetd.d.BAK/
		echo "Moved /etc/inetd.d/ to BAK set."
	fi
	if [ -a /etc/inetd.conf ] ; then
		mv /etc/inetd.conf /etc/BAK.inetd.conf.BAK
		echo "Moved /etc/inetd.conf to BAK set."
	fi
	echo "Export- and Inetd*-related files have been moved if applicable."
	
	if [ $menu_case -eq 1022 ] ; then
		av_rootkit
	fi
}

function av_rootkit {
	apt-get install clamav chkrootkit rkhunter lynis snort --yes
	echo "Updating ClamAV"
	freshclam
	echo "Running ClamAV"
	clamscan -r -i /
	echo -e "\nRunning ChkRootKit\n"
	chkrootkit -q
	echo -e "\nReturn to continue."
	read continue_case;
	case $continue_case in
		*) echo "" &> /dev/null ;;
	esac
	unset continue_case
	rkhunter -c
	if [ $? -eq 1 ] ; then
		echo "Shoot! RKHunter found/was an issue. Should I open the log for you to read?" # This appears to be really common...
		read rkhunter_case;
		case $rkhunter_case in
			1) less /var/log/rkhunter.log ;;
			y) less /var/log/rkhunter.log ;;
			Y) less /var/log/rkhunter.log ;;
			*) echo "" &> /dev/null ;;
		esac
		unset rkhunter_case
	fi
	lynis -c -Q
	echo -e "\nBack in the Hross-script world... read the above and return to continue."
	read continue_case;
	case $continue_case in
		*) echo "" &> /dev/null ;;
	esac
	unset continue_case
	echo "Running SNORT to daemonized mode, assuming interface eth0 " 
	snort -i eth0 -D
	
	if [ $menu_case -eq 1022 ] ; then
		closing_remarks
	fi
}

function closing_remarks {
	echo "Remember to look at the /etc/sudoers.d/ and /etc/sudoers"
	echo "Remember to look at all the /etc/cron.*/ files."
	echo "Remember to look at all the /etc/rc?.d/ files."
	echo "Remember to check firewall settings manually!"
	echo "Remember to check for hidden home dirs!"
	echo "Remember to check for media files and hacking tools!"
	echo "Remember to check for permissions of files around the system!"
	echo "Remember to check services!"
	echo "Remember to check -- well..."
	echo "NOTE: You should probably reboot now."
	
	exit 0 
}

function run_bastille {
	apt-get install bastille --yes
	bastille -c
}

function main_menu {
	# Main menu
	while true ; do
		echo "--------- MAIN MENU -----------"
		echo " 13 - OPTIONAL Exécutez la Bastille!"
		echo " 12 - Run all functions incrementing from option 1 (Recommended)"
		echo " 11 - Anti-Virus, chkrootkit"
		echo " 10 - /etc/exports/ and /etc/inetd* fixing"
		echo " 9 - Set lock and screen-saver to turn on"
		echo " 8 - Run a modified version of Bachrc.in's wack script"
		echo " 7 - Run BinBert's System Hardening Tips"
		echo " 6a - OPTIONAL upgrade system to newest version. May not be what CP wants! Runs as 6 -> 6a automatically."
		echo " 6 - Replace /etc/apt and update system (You may need to use Update Manager after)"
		echo " 5 - Secure Firewall"
		echo " 4 - Cron Jobs for all users"
		echo " 3 - Compare users to users in ./users.allowed and list groups each user is in"
		echo " 2 - Change passwords of all users 0 and UID between $l_clean and $l1_clean to password in ./pass.change"
		echo " 1 - Set password policies after installing libpam-cracklib"
		echo " 0 - Exit"
		read menu_case;
		case $menu_case in
			0) closing_remarks ;;
			1) pass_pol_set ;;
			2) change_passwords ;;
			3) compare_users ;;
			4) cron_jobs_funct ;;
			5) secure_firewall ;;
			6) update_system ;;
			6a) let menu_case=137
				update_system ;;
			7) binbert_script ;;
			8) bashrc_wack_script ;;
			9) lock_and_screensaver ;;
			10) exp_and_inetd ;;
			11) av_rootkit ;;
			12) let menu_case=1022
				pass_pol_set ;;
			13) run_bastille ;;
			*) main_menu ;;
		esac
	done
}

user_preli
check_hosts_file
# Now go to the main menu
clear
main_menu

# Shoot... something went wrong
echo "Uh-oh! Hross made a boo-boo with his programming!"
exit 1
